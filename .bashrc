### MEINE KONFIG ###

### übernehme pywal-thema in aktuelles terminal ###
#(cat ~/.cache/wal/sequences &)

### Zeige neofetch beim Start ###
neofetch

### CHANGE TITLE OF TERMINALS ###
 case ${TERM} in
  xterm*|rxvt*|xfce4*|gnome*|alacritty|termite|st)
    PROMPT_COMMAND='echo -ne "\033]0;${PWD/#$HOME/\~}\007"'
        ;;
  screen*)
    PROMPT_COMMAND='echo -ne "\033_${PWD/#$HOME/\~}\033\\"'
    ;;
 esac

### ARCHIVE EXTRACTION ###
# usage: ex <file>
ex ()
{
  if [ -f $1 ] ; then
    case $1 in
      *.tar.bz2)   tar xjf $1   ;;
      *.tar.gz)    tar xzf $1   ;;
      *.bz2)       bunzip2 $1   ;;
      *.rar)       unrar x $1   ;;
      *.gz)        gunzip $1    ;;
      *.tar)       tar xf $1    ;;
      *.tbz2)      tar xjf $1   ;;
      *.tgz)       tar xzf $1   ;;
      *.zip)       unzip $1     ;;
      *.Z)         uncompress $1;;
      *.7z)        7z x $1      ;;
      *.deb)       ar x $1      ;;
      *.tar.xz)    tar xf $1    ;;
      *.tar.zst)   unzstd $1    ;;
      *)           echo "'$1' cannot be extracted via ex()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}

## Export ###
export EDITOR=/usr/bin/nvim 	    	#$EDITOR use nvim
export VISUAL='nvim'					#$VISUAL use nvim
export HISTCONTROL=ignoreboth			#zeigt keine doppelten Ergebnisse in der History an
export MANPAGER="sh -c 'col -bx | bat -l man -p'"

if [ -d "$HOME/.bin" ] ;
  then PATH="$HOME/.bin:$PATH"
fi

if [ -d "$HOME/.local/bin" ] ;
  then PATH="$HOME/.local/bin:$PATH"
fi

### ignore upper and lowercase when TAB completion ###
bind "set completion-ignore-case on"

### Alias definitions. ###
if [ -f ~/.bash_aliases ]; then
	. ~/.bash_aliases
fi

### jump around in most used directories  ###
#./home/anthe/.bin/z.sh

### autojump ###
[[ -s /etc/profile.d/autojump.sh ]] && source /etc/profile.d/autojump.sh

### starship prompt ###
eval "$(starship init bash)"
