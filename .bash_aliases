###  eigene Aliase ###

## get fastest mirrors in your neighborhood
alias mirror="sudo reflector --country Germany --latest 5 --age 2 --fastest 5 --protocol https --sort rate --save /etc/pacman.d/mirrorlist"


## navigation
alias ..='cd ..'
alias ...='cd ../..'
alias .3='cd ../../..'
alias .4='cd ../../..'
alias .5='cd ../../../..'
alias .6='cd ../../../../..'


## Changing gnu apps
# "ls" to "exa"
alias ls='exa -alh --color=always --group-directories-first --color-scale --git' # my preferred listing
alias lt='exa -aT --color=always --group-directories-first' # tree listing
alias l.='exa -a | egrep "^\."'

# "rm" to "rmw"
alias rm='rmw'


## Programs
alias mi='micro'
alias smi='sudo micro'
alias vi='nvim'
alias vim='nvim'
alias rr="pix"


## confirmations
alias cp='cp -i'						  # confirm before overwriting something
alias mv='mv -i'						  # confirm before overwriting something
alias ln='ln -i'						  # confirm before overwriting something
#alias rm='rm -I'                          # confirm before deleting more than three files


## adding flags
alias mkdir='mkdir -pv'					  # create parents
alias df='df -h'						  # human-readable sizes
alias free='free -m'                      # show sizes in MB
alias wget='wget -c --hsts-file="$XDG_DATA_HOME/wget-hsts"'	 # --continue and save hsts-file in .local/share 
alias grep='grep -i --color=auto'		  # --ignore-case 
alias rg='rg -i --hidden'				  # --ignore-case include hidden 
alias history='history 1'
alias fd='fd -H -L -E .wine/ -E /run/timeshift -E /media/backup -E .local/ -E .cache/ -E .var/ -E .steam/' # --include-hidden --follow-symlinks --exclude-specific-folders
alias dc='dc -1'


## Searches for text in all files in the current folder
ftext ()
{
	# -i case-insensitive
	# -I ignore binary files
	# -H causes filename to be printed
	# -r recursive search
	# -n causes line number to be printed
	# optional: -F treat search term as a literal, not a regular expression
	# optional: -l only print filenames and not the matching lines ex. grep -irl "$1" *
	grep -iIHrn --color=always "$1" . | less -r
}


## ARCHIVE EXTRACTION
# usage: ex <file>
ex ()
{
  if [ -f $1 ] ; then
    case $1 in
      *.tar.bz2)   tar xjf $1   ;;
      *.tar.gz)    tar xzf $1   ;;
      *.bz2)       bunzip2 $1   ;;
      *.rar)       unrar x $1   ;;
      *.gz)        gunzip $1    ;;
      *.tar)       tar xf $1    ;;
      *.tbz2)      tar xjf $1   ;;
      *.tgz)       tar xzf $1   ;;
      *.zip)       unzip $1     ;;
      *.Z)         uncompress $1;;
      *.7z)        7z x $1      ;;
      *.deb)       ar x $1      ;;
      *.tar.xz)    tar xf $1    ;;
      *.tar.zst)   unzstd $1    ;;
      *)           echo "'$1' cannot be extracted via ex()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}


## Manage packages update easier
# Debian based Distros
if [ -f /usr/bin/apt ]; then
  alias upd='sudo apt update'
  alias upg='sudo apt update n sudo apt upgrade'
  alias ins='sudo apt install'
  alias uni='sudo apt purge'
  alias aptl='apt list --upgradable'
  alias clean='sudo apt autoremove && sudo apt clean'
  alias aptll='apt list | less'
fi

# Arch based Distros
if [ -f /usr/bin/pacman ]; then
  alias pacman='sudo pacman --color auto'
  alias upd='sudo pacman -Sy'
  alias vers='pacman -Qi'
  alias uni='sudo pacman -Rns'
  alias dirt='paccache -dvk1 && paccache -dvuk0 && sudo pacman -Qtdq'
  alias yupd='paru -Sy'
  alias upg='pacman -Sy archlinux-keyring --needed; paru -Syu --combinedupgrade --batchinstall; flatpak update'
  alias ins='paru -S --needed'
  alias installed='pkginstalldate --explicit'
  alias clean='pacclear && paru -c && flatpak uninstall --unused && yarn cache clean && pacman -Rns -'
fi

# Flatpaks
alias fupg='flatpak update'
alias fclean='flatpak uninstall --unused'


## Edit dotfiles
alias cbash='${EDITOR} ~/.bashrc'
alias calias='${EDITOR} ~/.bash_aliases'
alias caw='cd ~/.config/awesome/'
alias czsh='${EDITOR} ~/.config/zsh/.zshrc'


## Git
alias clone='git clone'
alias pull='git pull origin'
alias add='git add'
alias addup='git add -u'
alias status='git status'
alias commit='git commit -m'
alias cam='git commit -a -m'
alias push='git push origin'
alias addall='git add .'
#alias branch='git branch'
alias checkout='git checkout'
#alias fetch='git fetch'
#alias tag='git tag'
#alias newtag='git tag -a'

# Git commit shortcuts https://christitus.com/using-github-correctly/
gcom() {
	git add .
	git commit -m "$1"
	}

lazyg() {
	git add .
	git commit -m "$1"
	git push
}

# Git bare repo
alias dotfiles='/usr/bin/git --git-dir=$HOME/repos/dotfiles/ --work-tree=$HOME'
alias dclone='dotfiles clone'
alias dpull='dotfiles pull origin'
alias dadd='dotfiles add'
alias daddup='dotfiles add -u'
alias dstatus='dotfiles status'
alias dcommit='dotfiles commit -m'
alias dcam='dotfiles commit -a -m'
alias dpush='dotfiles push origin'
alias drm='dotfiles rm --cached'


## switch between shells
alias tobash="sudo chsh $USER -s /bin/bash && echo 'Now log out.'"
alias tozsh="sudo chsh $USER -s /bin/zsh && echo 'Now log out.'"


## Sonstiges
alias rb='reboot'
alias aus='shutdown now'
alias mmedien='udisksctl mount -b /dev/vg1/lv1 /media/medien'
alias umedien='udisksctl unmount -b /dev/vg1/lv1 /media/medien'
alias bpaper='sudo rsync -rav /media/daten/progs/paperless-ngx/ /media/backup/andere/paperless-ngx/'
#alias wcan='sudo modprobe v4l2loopback devices=1 max_buffers=2 exclusive_caps=1 card_label="VirtualCam #0" && ffmpeg -i http://192.168.178.26:8080/videofeed -f v4l2 -pix_fmt yuv420p /dev/video0'
#alias wcan='adb -s 7BKDU17727019678 shell "svc wifi enable" && adb shell input keyevent KEYCODE_POWER && sudo modprobe v4l2loopback devices=1 max_buffers=2 exclusive_caps=1 card_label="VirtualCam #0" && ffmpeg -i http://192.168.178.24:8080/videofeed -f v4l2 -pix_fmt yuv420p /dev/video0'
#alias wcan='adb -s 7BKDU17727019678 shell "svc wifi enable" && adb shell input keyevent KEYCODE_POWER && sudo modprobe v4l2loopback devices=1 max_buffers=2 exclusive_caps=1 card_label="VirtualCam #0" && ffmpeg -i http://192.168.178.24:8080/videofeed -f v4l2 -vcodec rawvideo -pix_fmt rgb24 /dev/video0'
#alias wcoff='adb -s 7BKDU17727019678 shell "svc wifi disable" && adb shell input keyevent KEYCODE_POWER'


## clean home
alias monerod='monerod --data-dir "$XDG_DATA_HOME"/bitmonero'
