### If not running interactively, don't do anything ###
[[ $- != *i* ]] && return


### Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
## Initialization code that may require console input (password prompts, [y/n]
## confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi


### Zeige neofetch beim Start ###
neofetch


### übernehme pywal-thema in aktuelles terminal ###
#(cat ~/.cache/wal/sequences &)


### Lines configured by zsh-newuser-install ###
unsetopt beep
bindkey -v
### End of lines configured by zsh-newuser-install ###


### The following lines were added by compinstall ###
zstyle :compinstall filename '/home/anthe/.zshrc'
autoload -Uz compinit
compinit
### End of lines added by compinstall ###


### add completion menu ###
zstyle ':completion:*' menu select
#setopt menu_complete
zstyle ':completion:*:*:cd:*' menu yes select
zstyle ':completion:*' format 'Completing %d'

#Completion Matching Control
zstyle ':completion:*' matcher-list '' 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=*' 'l:|=* r:|=*'


### History ###
#HISTFILE=~/.zsh_history
HISTSIZE=10000
SAVEHIST=10000

## Delete commands from history
# Accepts one history line number as argument.
# Alternatively, you can do `dc -1` to remove the last line.

dc () {
  # Prevent the specified history line from being saved.
  local HISTORY_IGNORE="${(b)$(fc -ln $1 $1)}"

  # Write out the history to file, excluding lines that match `$HISTORY_IGNORE`.
  fc -W

  # Dispose of the current history and read the new history from file.
  fc -p $HISTFILE $HISTSIZE $SAVEHIST

  # TA-DA!
  print "Deleted '$HISTORY_IGNORE' from history."
}

zshaddhistory() {
 [[ $1 != 'dc '* ]]
}

## append into history file
setopt INC_APPEND_HISTORY
## save only one command if 2 common are same and consistent
setopt HIST_IGNORE_ALL_DUPS
## add timestamp for each entry
setopt EXTENDED_HISTORY
## Do not require a leading ‘.’ in a filename to be matched explicitly.
setopt GLOB_DOTS
## Delte oldest duplicated history event first
setopt HIST_EXPIRE_DUPS_FIRST 


### Uncomment the following line to enable command auto-correction ###
ENABLE_CORRECTION="true"


### Alias ###
[[ -f ~/.bash_aliases ]] && . ~/.bash_aliases


### CHANGE TITLE OF TERMINALS ###
# Display $1 in terminal title.
function set-term-title() {
  emulate -L zsh
  if [[ -t 1 ]]; then
    print -rn -- $'\e]0;'${(V)1}$'\a'
  elif [[ -w $TTY ]]; then
    print -rn -- $'\e]0;'${(V)1}$'\a' >$TTY
  fi
}

# When a command is running, display it in the terminal title.
function set-term-title-preexec() {
  if (( P9K_SSH )); then
    set-term-title ${(V%):-"%n@%m: "}$1
  else
    set-term-title $1
  fi
}

# When no command is running, display the current directory in the terminal title.
function set-term-title-precmd() {
  if (( P9K_SSH )); then
    set-term-title ${(V%):-"%n@%m: %~"}
  else
    set-term-title ${(V%):-"%~"}
  fi
}

autoload -Uz add-zsh-hook
add-zsh-hook preexec set-term-title-preexec
add-zsh-hook precmd set-term-title-precmd


### environment variables (see in ~/.config/zsh/.zshenv)


### Keybindings ###
# create a zkbd compatible hash;
# # to add other keys to this hash, see: man 5 terminfo
typeset -g -A key

key[Home]="${terminfo[khome]}"
key[End]="${terminfo[kend]}"
key[Insert]="${terminfo[kich1]}"
key[Backspace]="${terminfo[kbs]}"
key[Delete]="${terminfo[kdch1]}"
key[Up]="${terminfo[kcuu1]}"
key[Down]="${terminfo[kcud1]}"
key[Left]="${terminfo[kcub1]}"
key[Right]="${terminfo[kcuf1]}"
key[PageUp]="${terminfo[kpp]}"
key[PageDown]="${terminfo[knp]}"
key[Shift-Tab]="${terminfo[kcbt]}"

# setup key accordingly
[[ -n "${key[Home]}"      ]] && bindkey -- "${key[Home]}"       beginning-of-line
[[ -n "${key[End]}"       ]] && bindkey -- "${key[End]}"        end-of-line
[[ -n "${key[Insert]}"    ]] && bindkey -- "${key[Insert]}"     overwrite-mode
[[ -n "${key[Backspace]}" ]] && bindkey -- "${key[Backspace]}"  backward-delete-char
[[ -n "${key[Delete]}"    ]] && bindkey -- "${key[Delete]}"     delete-char
[[ -n "${key[Up]}"        ]] && bindkey -- "${key[Up]}"         up-line-or-history
[[ -n "${key[Down]}"      ]] && bindkey -- "${key[Down]}"       down-line-or-history
[[ -n "${key[Left]}"      ]] && bindkey -- "${key[Left]}"       backward-char
[[ -n "${key[Right]}"     ]] && bindkey -- "${key[Right]}"      forward-char
[[ -n "${key[PageUp]}"    ]] && bindkey -- "${key[PageUp]}"     beginning-of-buffer-or-history
[[ -n "${key[PageDown]}"  ]] && bindkey -- "${key[PageDown]}"   end-of-buffer-or-history
[[ -n "${key[Shift-Tab]}" ]] && bindkey -- "${key[Shift-Tab]}"  reverse-menu-complete


### fuzzy xdg-open with fzf ###
fuzzy-xdg-open() {
  local output
  output=$(fzf --height 40% --reverse </dev/tty) && xdg-open ${(q-)output}
  zle reset-prompt
}

zle -N fuzzy-xdg-open
bindkey '^o' fuzzy-xdg-open

# Finally, make sure the terminal is in application mode, when zle is
# active. Only then are the values from $terminfo valid.
if (( ${+terminfo[smkx]} && ${+terminfo[rmkx]} )); then
	autoload -Uz add-zle-hook-widget
	function zle_application_mode_start { echoti smkx }
	function zle_application_mode_stop { echoti rmkx }
	add-zle-hook-widget -Uz zle-line-init zle_application_mode_start
	add-zle-hook-widget -Uz zle-line-finish zle_application_mode_stop
fi


###  Load ; should be last ###
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source /usr/share/zsh/plugins/zsh-autopair/autopair.zsh
source /usr/share/zsh/plugins/zsh-sudo/sudo.plugin.zsh
source /usr/share/zsh/plugins/zsh-z/zsh-z.plugin.zsh
source /usr/share/zsh-theme-powerlevel10k/powerlevel10k.zsh-theme
source /home/anthe/.config/zsh/plugins/gruvbox-material-dark.zsh
source /home/anthe/.config/zsh/plugins/zle-fzf.plugin.zsh
source /usr/share/fzf/key-bindings.zsh
source /usr/share/fzf/completion.zsh
autoload zmv


### Bind fzf crtl-t to crtl-f ###
bindkey -r '^T'
bindkey -r '^F'
bindkey '^F' fzf-file-widget


### To customize prompt, run `p10k configure` or edit ~/.p10k.zsh. ###
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh
