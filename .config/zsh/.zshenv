###  environment variables ###
export EDITOR=/usr/bin/nvim 	    		#$EDITOR use nvim
export VISUAL='nvim'			    		#$VISUAL use nvim
export MANPAGER="sh -c 'col -bx | bat -l man -p'"
export LESSHISTFILE=-                       #deaktiviert .lesshst
export TERMINAL='alacritty'

### PATH ###
if [ -d "$HOME/.bin" ] ;
  then PATH="$HOME/.bin:$PATH"
fi

if [ -d "$HOME/.local/bin" ] ;
  then PATH="$HOME/.local/bin:$PATH"
fi

### fzf ###
export FZF_DEFAULT_OPTS='--height 40% --layout=reverse --border'
export FZF_DEFAULT_COMMAND='fd -H -L -E .wine/ -E /run/timeshift -E /media/backup -E .local/ -E .cache/ -E .var/ -E .steam/ . $HOME'
export FZF_ALT_C_COMMAND='fd --type d --hidden --follow -E .wine/ -E /run/timeshift -E /media/backup -E .local/ -E .cache/ -E .var/ -E .steam/ . $HOME'
export FZF_CTRL_T_COMMAND="$FZF_DEFAULT_COMMAND"

### user directories ###
export XDG_CONFIG_HOME=$HOME/.config
export XDG_CACHE_HOME=$HOME/.cache
export XDG_DATA_HOME=$HOME/.local/share
export XDG_STATE_HOME=$HOME/.local/state

### clean ~ ###
export ZDOTDIR="$HOME"/.config/zsh
export HISTFILE="$XDG_STATE_HOME"/zsh/history
export HISTCONTROL=ignoreboth:erasedups		#zeigt keine doppelten Ergebnisse in der History an
export _Z_DATA=$XDG_DATA_HOME/z
export WINEPREFIX="$XDG_DATA_HOME"/wine
export GTK2_RC_FILES="$XDG_CONFIG_HOME"/gtk-2.0/gtkrc
export CARGO_HOME="$XDG_DATA_HOME"/cargo
