require("anthe.base") -- General Settings
require("anthe.highlights") -- Colourscheme and other highlights
require("anthe.maps") -- Keymaps
require("anthe.plugins") -- Plugins
require("anthe.bootstrap") -- Packer Auto-Installer
