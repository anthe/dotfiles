-------------------------------------------------
-- Neovim website: https://neovim.io/
-------------------------------------------------

-------------------------------------------------
-- KEYBINDINGS
-------------------------------------------------

local function map(m, k, v)
	vim.keymap.set(m, k, v, { silent = true })
end

-- Mimic shell movements
map("i", "<C-E>", "<ESC>A")
map("i", "<C-A>", "<ESC>I")

-- Load recent sessions
map("n", "<leader>sl", "<CMD>SessionLoad<CR>")

-- Keybindings for telescope
map("n", "<leader>fr", "<CMD>Telescope oldfiles<CR>")
map("n", "<leader>ff", "<CMD>Telescope find_files<CR>")
map("n", "<leader>fb", "<CMD>Telescope file_browser<CR>")
map("n", "<leader>fw", "<CMD>Telescope live_grep<CR>")

-- Tabedit keybinds
vim.keymap.set("n", "<leader>tt", ":tabnew<CR>", { desc = "new tab" })
vim.keymap.set("n", "<leader>tc", ":tabclose<CR>", { desc = "close tab" })
vim.keymap.set("n", "<leader>t1", "1gt<CR>", { desc = "1. tab" })
vim.keymap.set("n", "<leader>t2", "2gt<CR>", { desc = "2. tab" })
vim.keymap.set("n", "<leader>t3", "3gt<CR>", { desc = "3. tab" })
vim.keymap.set("n", "<leader>t4", "4gt<CR>", { desc = "4. tab" })
vim.keymap.set("n", "<leader>t5", "5gt<CR>", { desc = "5. tab" })

-- other vim shortcuts
-- map source
vim.keymap.set("n", "<leader>vs", ":w<CR>:so %<CR>", { desc = "source" })
-- Map insert one char
vim.keymap.set("n", "<leader>vi", "i_<ESC>r", { desc = "insert one char" })
-- Map global subsitute
vim.keymap.set("n", "<leader>vg", ":%s///g<Left><Left><Left>", { desc = "global substitute" })

-- easier split navigations
map("n", "<F3>", "<CMD>:vsp<CR>")
map("n", "<C-H>", "<C-W><C-H>")
map("n", "<C-L>", "<C-W><C-L>")

-- toggle undotree
map("n", "<F5>", "<CMD>:UndotreeToggle<CR>")
map("n", "<F6>", "<CMD>:NERDTreeToggle<CR>")
