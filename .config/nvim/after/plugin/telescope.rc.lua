-------------------------------------------------
-- Neovim website: https://neovim.io/
-------------------------------------------------
--
local status, telescope = pcall(require, "telescope")
if not status then
	return
end

telescope.setup({
    pickers = {
        find_files = {
            find_command={
            'fd',
            '--type',
            'file',
            '--hidden',
            '--exclude',
            '.local/',
            '--exclude',
            '.cache/',
            '--exclude',
            '.var/',
            '--strip-cwd-prefix',
            },
        },
    },
	extensions = {
		file_browser = {
			theme = "ivy",
			-- disables netrw and use telescope-file-browser in its place
			hijack_netrw = true,
            hidden = true,
			mappings = {
				["i"] = {
					-- your custom insert mode mappings
				},
				["n"] = {
					-- your custom normal mode mappings
				},
			},
		},
	},
})

telescope.load_extension("file_browser")
