-------------------------------------------------
-- Neovim website: https://neovim.io/
-------------------------------------------------

local status, which_key = pcall(require, "which-key")
if not status then
	return
end

which_key.setup({
	-- your configuration comes here
	-- or leave it empty to use the default settings
})

local wk = require("which-key")

wk.register({
  f = {
    name = "find", -- optional group name
  },
  s = {
    name = "session",
  },
  t = {
    name = "tabs",
  },
  v = {
    name = "my vim shortcuts",
  },
}, {prefix = "<leader>"} )
